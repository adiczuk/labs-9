/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author st
 */
public class Fabryka implements ConsultationListFactory {

    private ConsultationList lista;
    
    @Override
    public ConsultationList create() {
        return new ListaKonsultacji();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        Object wynik = null;
        
        if(deserialize){
        try{
            XMLDecoder decoder = new XMLDecoder(
                    new BufferedInputStream(
                    new FileInputStream("save.xml")));
            
            wynik = decoder.readObject();
            decoder.close();
        }
        catch(FileNotFoundException e) {
            System.out.print(e.getMessage());
        }   
        }        
        return (ConsultationList)wynik;
    }

    @Override
    public void save(ConsultationList consultationList) {
        
        try{
            XMLEncoder encoder = new XMLEncoder(
                    new BufferedOutputStream(
                    new FileOutputStream("save.xml")));

            encoder.writeObject(this.lista);
            encoder.close();
        }
        catch(FileNotFoundException e){
            System.out.print(e.getMessage());
        }
        }
             
}
