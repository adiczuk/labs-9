/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class Konsultacja implements Consultation {

    private final VetoableChangeSupport vcs = new VetoableChangeSupport(this);
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    
    private String student;
    private Term term;
    
    public Konsultacja(){}
    
    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        Term oldTerm = this.term;
        vcs.fireVetoableChange("Term", oldTerm, term);
        this.term = term;
        pcs.firePropertyChange("Term", oldTerm, term);
        
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {    
        if(minutes <= 0) minutes=0;  
        int oldDuration = this.term.getDuration();
        this.vcs.fireVetoableChange("Term", oldDuration, oldDuration + minutes);
        this.term.setDuration(this.term.getDuration() + minutes);
        this.pcs.firePropertyChange("Term", oldDuration, oldDuration + minutes);
    }
    
}
