/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

/**
 *
 * @author st
 */
public class ListaKonsultacji implements ConsultationList {    
    
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    
    private Consultation[] lista;

    public ListaKonsultacji()
    {
        this.lista = new Consultation[]{};
    }
    
    @Override
    public int getSize() {
        return this.lista.length;
    }

    @Override
    public Consultation[] getConsultation() {
        return this.lista;
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.lista[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {       
        
        for(int i=0; i < this.lista.length;i++)
        {
            if((consultation.getEndDate().after(this.lista[i].getBeginDate()) && consultation.getBeginDate().before(this.lista[i].getBeginDate()))
                    ||(consultation.getBeginDate().before(this.lista[i].getEndDate()) && consultation.getEndDate().after(this.lista[i].getBeginDate())))
            {
                throw new PropertyVetoException("Blad", null);
            }          
        }   
        
        Consultation[] nowa = Arrays.copyOf(this.lista,  this.lista.length + 1);
        Consultation[] stara = this.lista;
        nowa[nowa.length - 1] = consultation;        
        this.lista = nowa;       
        pcs.firePropertyChange("consultation", stara, nowa);
       
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
       this.pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }
    
}
